package sks.sergey;

/*
Snake (Питон, Удав, Змейка и др.) — компьютерная игра, возникшая в середине или в конце 1970-х.

Игрок управляет длинным, тонким существом, напоминающим змею, которое ползает по плоскости
ограниченной стенками, собирая еду, избегая столкновения с собственным хвостом и краями игрового поля.
Каждый раз, когда змея съедает кусок пищи, она становится длиннее, что постепенно усложняет игру.
Игрок управляет направлением движения головы змеи ( 4 направления: вверх, вниз, влево, вправо),
а хвост змеи движется следом. Игрок не может остановить движение змеи.
 */

import javax.swing.*;

/**
 * Created by SKS on 18.09.2017.
 */
public class Main extends JFrame{
    public Main(){
        setTitle("Game 2D Snake");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400,425);
        setLocation(200,200);
        add(new GameField());
        setVisible(true);
    }

    public static void main(String[] args) {
        Main newGame = new Main();
    }
}
